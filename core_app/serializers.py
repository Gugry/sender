from rest_framework import serializers
from core_app.models import Mailing, Client, Message


class MailingSerializer(serializers.ModelSerializer):
    """
    Mailing serializer
    """
    class Meta:
        model = Mailing
        fields = ['url', 'pk', 'date_start', 'message_text', 'filter_tag', 'filter_mobile_operator_code', 'date_end']


class ClientSerializer(serializers.ModelSerializer):
    """
    Client serializer
    """
    class Meta:
        model = Client
        fields = ['url', 'pk', 'phone_num', 'mobile_operator_code', 'tag', 'time_zone']


class StatisticsSerializer(serializers.ModelSerializer):
    """
    General statistics of the created mailings and the number of sent messages on them
    with grouping by statuses
    """
    Mail_count = serializers.SerializerMethodField()
    just_created = serializers.SerializerMethodField()
    successfully_sent = serializers.SerializerMethodField()
    sending_error = serializers.SerializerMethodField()
    overdue = serializers.SerializerMethodField()
    detail_statistics = serializers.HyperlinkedIdentityField(view_name='statistics-details')

    class Meta:
        model = Mailing
        fields = ['detail_statistics',
                  'pk',
                  'message_text',
                  'Mail_count',
                  'just_created',
                  'successfully_sent',
                  'sending_error',
                  'overdue']

    def get_Mail_count(self, obj):
        return Message.objects.filter(mailing=obj.pk).count()

    def get_just_created(self, obj):
        return Message.objects.filter(mailing=obj.pk).filter(status=0).count()

    def get_successfully_sent(self, obj):
        return Message.objects.filter(mailing=obj.pk).filter(status=1).count()

    def get_sending_error(self, obj):
        return Message.objects.filter(mailing=obj.pk).filter(status=2).count()

    def get_overdue(self, obj):
        return Message.objects.filter(mailing=obj.pk).filter(status=3).count()


class DetailStatisticsSerializer(serializers.ModelSerializer):
    """
    Detailed statistics of sent messages for a defined mailing list
    """

    mailing = serializers.SlugRelatedField(
        read_only=True,
        slug_field='message_text'
    )

    class Meta:
        model = Message
        fields = ['mailing', 'date_create', 'date_send', 'status', 'client', ]
